$(document).ready(function(){
	
	//carousel init
	$('.carousel').slick({
		dots: false,
		slidesToShow: 1,
		infinite: true,
		speed: 500,
		fade: true,
		autoplay: true,
  		autoplaySpeed: 6000,
  		adaptiveHeight: false,
  		pauseOnHover: true,
  		arrows: false,
		cssEase: 'linear'
	});

	//fit text - responsive font size
	$('.carousel h3').fitText(0.9, { minFontSize: '20px', maxFontSize: '47px' });
	$('.counter').fitText(0.35, { minFontSize: '20px', maxFontSize: '107px' });

	//svg animation
    $path = $('#animated_svg_border').find('path');
    $path
        .css('cursor', 'pointer')
        .attr({
            'stroke-dasharray' : $path.get(0).getTotalLength(),
            'stroke-dashoffset' : $path.get(0).getTotalLength()
        });
	$path.velocity({'stroke-dashoffset': 0}, { duration: 3000, delay: 10 });	
   
	var firstSlideP = $('.slick-current[data-slick-index="0"]').find('p');

	var properties = {
	   	opacity : 0.3
	};

	//white background 
	$('.white_transparent').pulse(properties, {
	  	duration : 1000,
	  	pulses   : 4,
	  	interval : 1000
	});

	//on page load show the i elements one by one
	setTimeout(function(){
		//first slide i animations
		$('.carousel .slick-current').find('i').velocity('fadeIn', {
			stagger: 200,
	        delay: 100,
	        complete : function(){
	            
	        }
	    })
	}, 3000);

	function firstSlideAnimations(){

		setTimeout(function(){

		    //first slide p animation
			firstSlideP.velocity("fadeIn", { display: null, duration: 1000, delay: 1000}); 
			
			//pulse effect
			var properties = {
			   opacity : 0.2
			};
 
			firstSlideP.find('.btn').pulse(properties, {
			  	duration : 1400,
			  	pulses   : 4,
			  	interval : 1000
			});
		}, 3000);
	}

	firstSlideAnimations();

	//on carousel change do next things
	$('.carousel').on('afterChange', function(){

		//do the counter 
		if($('.slick-current .counter').length){
	  		$('.counter').counterUp({
		        delay: 100, 
		        time: 2000
	    	});
    	}

    	//animate the elements - show them
    	$('.slick-current').find('i').velocity("fadeIn", { display: null, stagger: 200, delay: 100 });
  		
  		//on slide change fade out i elements which are not in the curent slide, to prepared them for the next swipe
  		$('.slick-slide').not('.slick-current').find('i').velocity({ opacity: 0 }, { duration: 0, delay: 0 });
	
		//set different displaying time in different slider items for p elements
  		$('.slick-current[data-slick-index="0"]').find('p').velocity("fadeIn", { display: null, duration: 1000, delay: 1000}); 
  		$('.slick-current[data-slick-index="1"]').find('p').velocity("fadeIn", { display: null, duration: 1000, delay: 0}); 
  		$('.slick-current[data-slick-index="2"]').find('p').velocity("fadeIn", { display: null, duration: 1000, delay: 2000});
  		
  		//on slide change fade out p elements which are not in the curent slide, to prepared them for the next swipe
  		$('.slick-slide').not('.slick-current').find('p').velocity({ opacity: 0 }, { duration: 0, delay: 0 });
	 
    	firstSlideAnimations();
	});
	
});