var gulp 			= require('gulp');
var jshint 			= require('gulp-jshint');
var uglify 			= require('gulp-uglifyjs');
var concat 			= require('gulp-concat');
var stylus 			= require('gulp-stylus');
var event_stream 	= require('event-stream');
var rename 			= require('gulp-rename');
var modernizr 		= require('gulp-modernizr');
var cleanCSS 		= require('gulp-clean-css');
var autoprefixer 	= require('gulp-autoprefixer');
var sourcemaps 		= require('gulp-sourcemaps');
var imagemin 		= require('gulp-imagemin');
var pngquant 		= require('imagemin-pngquant');
var autoprefixer 	= require('gulp-autoprefixer');
var browserSync 	= require('browser-sync');
var reload      	= browserSync.reload;
var connectPHP 		= require('gulp-connect-php');


// ////////////////////////////////////////////////
// Paths to source files
// paths haves changed a bit.
// ///////////////////////////////////////////////
var paths = {
    html:['./*.php'],
    css:['css/stylus/*.styl'],
    script:['js/*.js']
};

// ////////////////////////////////////////////////
// HTML task
// ///////////////////////////////////////////////
gulp.task('html', function(){
    gulp.src(paths.html)
    .pipe(reload({stream:true}));
});

// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////
gulp.task('browserSync', function() {
    browserSync({
        proxy:'127.0.0.1',
        port:8080
    });
});

// /////////////////////////////////////////////////
// PHP task
// ////////////////////////////////////////////////
gulp.task('php', function(){
    connectPHP.server({ base: './', keepalive:true, hostname: 'localhost', port:8080, open: false});
});


gulp.task('imagemin', function () {
    return gulp.src('./images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./images'));
});


// /////////////////////////////////////////////////
// JS testing
// ////////////////////////////////////////////////
gulp.task('lint', function() {
	return gulp.src('src/js/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});


// /////////////////////////////////////////////////
// errors
// ////////////////////////////////////////////////
function handleError(err) {
	console.log(err.toString());
	this.emit('end');
}

// /////////////////////////////////////////////////
// styles
// ////////////////////////////////////////////////
gulp.task('styles', function() {
  var stylusStream = gulp.src('./css/stylus/**.styl')
    .pipe(sourcemaps.init())
	.pipe(stylus({
		compress: false,
		'include css': true
		// linenos: true
	}).on("error", handleError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(sourcemaps.write());

  return event_stream.merge(stylusStream, gulp.src(
			[
				//'./styles/css/**.css'
			]
		)
	)
    .pipe(concat('all.min.css'))
	.pipe(cleanCSS({
		compatibility: 'ie8'
	}))
    .pipe(gulp.dest('./dest'))
	.pipe(reload({stream:true}));
});

// /////////////////////////////////////////////////
// scripts
// ////////////////////////////////////////////////
gulp.task('scripts', function() {
	return gulp.src(
			[
				'node_modules/gulp-modernizr/build/modernizr-custom.js',
				'node_modules/velocity-animate/velocity.js',
				'node_modules/velocity-animate/velocity-ui.js',
				'node_modules/slick-carousel/slick/slick.js',
				'./js/*.js'
			]
		)
		.pipe(concat('all.js'))
		.pipe(gulp.dest('dest'))
		.pipe(rename('all.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dest'))
		.pipe(reload({stream:true}));
});

// /////////////////////////////////////////////////
// modernizr
// ////////////////////////////////////////////////
gulp.task('modernizr', function() {
	return gulp.src('js/modernizr-config.json')
		.pipe(modernizr())
		.pipe(gulp.dest('dest'));
});

// /////////////////////////////////////////////////
// watch for changes
// ////////////////////////////////////////////////
gulp.task('watch', function(){	
	gulp.watch('css/stylus/*.styl', ['styles']);
	gulp.watch('./js/*.js', ['scripts']);
   
	gulp.watch(paths.css, ['styles']);
    gulp.watch(paths.script, ['scripts']);
    gulp.watch(paths.html, ['html']);	
});

// /////////////////////////////////////////////////
// default tasks
// ////////////////////////////////////////////////
gulp.task('default', ['watch', 'browserSync', 'styles', 'modernizr', 'scripts', 'php']);
